from django.test import TestCase
from django.utils import timezone
from .models import Indonesia, Dunia

# Create your tests here.
class Tests(TestCase):
    timezone.activate("Asia/Jakarta")

    def test_url(self):
        response = self.client.get('')
        self.assertEquals(response.status_code, 200)

    def test_html(self):
        response = self.client.get('')
        html_resp = response.content.decode('utf8')
        self.assertTemplateUsed(response, 'home.html')
        self.assertIn('Indonesia', html_resp)
        self.assertIn('Dunia', html_resp)

    def test_models(self):
        Indonesia.objects.create(
            total = 2,
            sembuh = 1,
            meninggal = 1,
            tanggal = timezone.localdate()
        )
        count1 = Indonesia.objects.all().count()
        Dunia.objects.create(
            total = 2,
            sembuh = 1,
            meninggal = 1,
            tanggal = timezone.localdate()
        )
        count2 = Dunia.objects.all().count()
        self.assertEquals(count1, 1)
        self.assertEquals(count2, 1)