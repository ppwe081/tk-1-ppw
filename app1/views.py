from django.shortcuts import render
from django.utils import timezone
import requests
from .models import Indonesia, Dunia

def home(request):
    timezone.activate("Asia/Jakarta")

    objInd = None
    objD = None

    if not Indonesia.objects.all().exists():
        dataInd = requests.get("https://api.kawalcorona.com/indonesia")
        dataDP = requests.get("https://api.kawalcorona.com/positif")
        dataDS = requests.get("https://api.kawalcorona.com/sembuh")
        dataDM = requests.get("https://api.kawalcorona.com/meninggal")

        datajInd = dataInd.json()
        datajDP = dataDP.json()
        datajDS = dataDS.json()
        datajDM = dataDM.json()

        # print(datajInd)
        # print(datajDP)
        # print(datajDS)
        # print(datajDM)

        objInd = Indonesia.objects.create(
            total = datajInd[0]["positif"],
            sembuh = datajInd[0]["sembuh"],
            meninggal = datajInd[0]["meninggal"],
            tanggal = timezone.localdate()
        )

        objD = Dunia.objects.create(
            total = datajDP["value"],
            sembuh = datajDS["value"],
            meninggal = datajDM["value"],
            tanggal = timezone.localdate()
        )

    if Indonesia.objects.all().exists():
        dataInd = requests.get("https://api.kawalcorona.com/indonesia")
        dataDP = requests.get("https://api.kawalcorona.com/positif")
        dataDS = requests.get("https://api.kawalcorona.com/sembuh")
        dataDM = requests.get("https://api.kawalcorona.com/meninggal")

        datajInd = dataInd.json()
        datajDP = dataDP.json()
        datajDS = dataDS.json()
        datajDM = dataDM.json()   

        objInd = Indonesia.objects.all()
        objInd.total = datajInd[0]["positif"]
        objInd.sembuh = datajInd[0]["sembuh"]
        objInd.meninggal = datajInd[0]["meninggal"]
        objInd.tanggal = timezone.localdate()

        objD = Dunia.objects.all()
        objD.total = datajDP["value"]
        objD.sembuh = datajDS["value"]
        objD.meninggal = datajDM["value"]
        objD.tanggal = timezone.localdate()

    context = {
        "dataInd" : objInd,
        "dataD" : objD,
        "tanggal" : timezone.localdate(),
    }


    return render(request, 'home.html', context)
