from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import listrelawan, detailrelawan, deleterelawan
import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'app4')))

from app4.models import Relawan
# Create your tests here.

class Tests(TestCase):
    def test_listrelawan_url_is_exist(self):
        response = Client().get('/listrelawan/')
        self.assertEqual(response.status_code, 200)

    def test_listrelawan_using_app5_template(self):
        response = Client().get('/listrelawan/')
        self.assertTemplateUsed(response, 'app5.html')

    def test_listrelawan_using_func_listrelawan(self):
        found = resolve('/listrelawan/')
        self.assertEqual(found.func, listrelawan)

    def test_listrelawan_detailurl_url_is_exist(self):
        Relawan.objects.create(
            nama='abc' , 
            nama_panggilan='a',
            no_hp='0812121212',
            email='abc@gmail.com',
            identitas='KTP',
            no_identitas='123123123123',
            alamat='asadadasasadasdasda',
            jenis_kelamin='Perempuan',
            kota_kelahiran='london',
            tanggal_lahir='2001-12-12',
            agama='Islam',
            status='Belum Menikah',
            pekerjaan='ingfluenza',
            peminatan='Relawan Medis',
            saya_menyetujui_syarat_dan_ketentuan_yang_berlaku=True,
            )
        response = Client().get('/listrelawan/detailrelawan/1')
        self.assertEqual(response.status_code, 200)

    def test_listrelawan_detailrelawan_using_detailrelawan_template(self):
        Relawan.objects.create(
            nama='abc' , 
            nama_panggilan='a',
            no_hp='0812121212',
            email='abc@gmail.com',
            identitas='KTP',
            no_identitas='123123123123',
            alamat='asadadasasadasdasda',
            jenis_kelamin='Perempuan',
            kota_kelahiran='london',
            tanggal_lahir='2001-12-12',
            agama='Islam',
            status='Belum Menikah',
            pekerjaan='ingfluenza',
            peminatan='Relawan Medis',
            saya_menyetujui_syarat_dan_ketentuan_yang_berlaku=True,
            )
        response = Client().get('/listrelawan/detailrelawan/1')
        self.assertTemplateUsed('detailrelawan.html')

    def test_listrelawan_detailrelawan_using_func_detailrelawan(self):
        Relawan.objects.create(
            nama='abc' , 
            nama_panggilan='a',
            no_hp='0812121212',
            email='abc@gmail.com',
            identitas='KTP',
            no_identitas='123123123123',
            alamat='asadadasasadasdasda',
            jenis_kelamin='Perempuan',
            kota_kelahiran='london',
            tanggal_lahir='2001-12-12',
            agama='Islam',
            status='Belum Menikah',
            pekerjaan='ingfluenza',
            peminatan='Relawan Medis',
            saya_menyetujui_syarat_dan_ketentuan_yang_berlaku=True,
            )
        
        found = resolve('/listrelawan/detailrelawan/1')
        self.assertEqual(found.func, detailrelawan)
