from django.shortcuts import render
from django.http import HttpResponseRedirect
import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'app4')))

from app4.models import Relawan
# Create your views here.

def listrelawan(request):
    relawans = Relawan.objects.all()
    response = {}
    response['relawans'] = relawans

    return render(request, 'app5.html', response)

def detailrelawan(request, pk):
    relawans = Relawan.objects.get(id = pk)
    response = {'relawans' : relawans}
    return render(request, 'detailrelawan.html', response)

def deleterelawan(request, pk):
    relawans = Relawan.objects.get(id = pk)
    if request.method == "POST":
        relawans.delete()
        return HttpResponseRedirect('/listrelawan')
    response = {'relawans' : relawans}
    return render(request, 'deleterelawan.html', response)
