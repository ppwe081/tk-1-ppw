from django.shortcuts import render,redirect
from .forms import relawanForm
from .models import Relawan
from django.core.mail import send_mail

def relawan(request):
    if request.method == "POST":
        form = relawanForm(request.POST)
        if form.is_valid():
            formrelawan = Relawan()
            formrelawan.nama = form.cleaned_data['nama']
            formrelawan.nama_panggilan = form.cleaned_data['nama_panggilan']
            formrelawan.no_hp = form.cleaned_data['no_hp']
            formrelawan.email = form.cleaned_data['email']
            formrelawan.identitas = form.cleaned_data['identitas']
            formrelawan.no_identitas = form.cleaned_data['no_identitas']
            formrelawan.alamat = form.cleaned_data['alamat']
            formrelawan.jenis_kelamin = form.cleaned_data['jenis_kelamin']
            formrelawan.kota_kelahiran = form.cleaned_data['kota_kelahiran']
            formrelawan.tanggal_lahir = form.cleaned_data['tanggal_lahir']
            formrelawan.agama = form.cleaned_data['agama']
            formrelawan.status = form.cleaned_data['status']
            formrelawan.pekerjaan = form.cleaned_data['pekerjaan']
            formrelawan.peminatan = form.cleaned_data['peminatan']
            formrelawan.save()
            send_mail('Selamat Datang Relawan Baru!',
            'Halo! Terimakasih telah melakukan registrasi menjadi relawan kami.\nKami akan segera menghubungi anda bila ada pekerjaan.\nSalam Kebaikan :)\n-Kelompok 8-',
            'ppwkelompok8@gmail.com',
            [formrelawan.email],
            fail_silently=False)
            
        return redirect('/relawan/success')
    else:
        relawanall = Relawan.objects.all()
        form = relawanForm()
        response = {"relawanall":relawanall, 'form' : form}
        return render(request,'app4.html',response)

def success(request):
    return render(request, 'success.html')