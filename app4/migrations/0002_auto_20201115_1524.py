# Generated by Django 3.1.1 on 2020-11-15 08:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app4', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='relawan',
            name='saya_menyetujui_syarat_dan_ketentuan_yang_berlaku',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='relawan',
            name='no_hp',
            field=models.IntegerField(),
        ),
    ]
