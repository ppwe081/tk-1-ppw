# Generated by Django 3.1.1 on 2020-11-20 13:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app4', '0002_auto_20201115_1524'),
    ]

    operations = [
        migrations.AlterField(
            model_name='relawan',
            name='no_hp',
            field=models.CharField(default='', max_length=200),
        ),
    ]
