# TK1

Tugas Kelompok 1 - E08

Anggota Kelompok:
1. Dafiq Achmad - 1906308343
2. Muhammad Ikhsan Asa Pambayun - 1906350830
3. Aziz Jabbar Shiddiq - 1906353611
4. Efrado Suryadi - 1906350995
5. Muhammad Daffa Yudhistira Dwipayana - 1906353441

Link wireframe :
1. https://wireframe.cc/REhvWu  
2. https://wireframe.cc/RlnpjJ
3. https://wireframe.cc/NX3gU7
4. https://wireframe.cc/H0Uj6m
5. https://wireframe.cc/TwSlVP

Link prototype : https://www.figma.com/file/eR4uETTyCRMy88SAogmVaZ/tk1?node-id=1%3A2

Link heroku : https://tugaskelompok08.herokuapp.com/

Persona : https://docs.google.com/document/d/1xHIcr6ZHQJyznfOO3EAI8ko4DkrtcKOI7Kz9kQyp-dA/edit?usp=sharing

Cerita app :

    Pada masa pandemi seperti ini, banyak orang yang tidak beruntung seperti kita. 
    Baik kehilangan perkerjaan, tidak dapat mencari uang, bahkan makan saja kesulitan. 
    Karena terinspirasi dari hal itu, kita mencoba membuat sebuah app yang dapat mengumpulkan 
    orang-orang yang ingin menyisihkan sebagian harta maupun bendanya kepada orang orang yang 
    tidak mampu dan membutuhkan, selain itu juga kita membuka kesempatan bagi orang-orang yang 
    ingin menjadi relawan untuk sekiranya membatu mendistribusikan donasi yang telah kita 
    dapatkan kepada orang-orang yang berhak mendapatkannya.

Fitur :

    1. Home
        - menampilkan data jumlah korona aktif yang ada di indonesia dan dunia saat ini
        - menampilkan data jumlah kesembuhan yang ada di indonesia dan dunia saat ini
        - menampilkan data jumlah kematian yang ada di indonesia dan dunia saat ini

    2. Sumbangan uang
        - donatur dapat memberikan sumbangan sesuai keinginannya
        - donatur memilih rekening yang digunakan
        - setelah itu mengkonfirmasi pembayaran yang akan dilakukan

    3. Sumbangan barang
        - donatur dapat memberikan sumbangan sesuai keinginannya
        - donatur memilih lokasi penjemputan ataupun pengiriman barang
        - setelah itu mengkonfirmasi pengiriman yang dilakukan

    4. Daftar relawan
        - calon relawan mengisi formulir pendaftaran
        - akan dikirimkan email konfirmasi bahwa pendaftar telah terdaftar
        
    5. List relawan
        - dapat melihat daftar relawan yang telah terdaftar
        - dapat menghapus keanggotaan relawan yang terdaftar