from django.urls import path

from . import views

app_name = 'app3'

urlpatterns = [
    path('', views.form_barang, name='form_barang'),
    path('success', views.success, name='success'),
    path('confirm', views.confirm, name='confirm')
]
