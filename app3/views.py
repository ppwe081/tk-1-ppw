from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import FormBarang


# Create your views here.
def form_barang(request):
    if(request.method == "POST"):
        form = FormBarang(request.POST)
        context = {'form' : form}
        if form.is_valid():
            #form.save()
            return render(request, 'app3/konfirmasi.html', context)
            
            #return HttpResponseRedirect('/donasibarang/success')
    else:
        form = FormBarang()
       
    context = {'form' : form}
    return render(request, "app3/donasibarang.html", context)

def success(request):
    return render(request, 'app3/sukses.html')

def confirm(request):
    if(request.method == "POST"):
        form=FormBarang(request.POST)
        if form.is_valid():
            form.save()
        return HttpResponseRedirect('/donasibarang/success')