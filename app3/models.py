from django.db import models

# Create your models here.
BARANG = (
    ('', 'Choose...'),
    ('Pakaian', 'Pakaian'),
    ('Sembako', 'Sembako'),
    ('Lainnya', 'Lainnya')
)
class Barang(models.Model):
    nama_donatur = models.CharField(max_length=50)
    no_handphone = models.CharField(max_length=50)
    barang = models.CharField(max_length=50, choices=BARANG)
    date = models.DateField()
    place = models.CharField(max_length=50)
