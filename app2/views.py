from django.shortcuts import render, redirect, reverse
from .models import Isi
from .forms import FormUtama
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
def FormPelanggan(request):
    if request.method == "POST":
        form_utama = FormUtama(request.POST)
        context = {
        "formu" : form_utama
        }
        if form_utama.is_valid():
            form_utama.save()
            return render(request, 'KonfirmasiPembayaran.html', context)

    context = {
        "formu" : FormUtama(),
        "is_tambah_form" : True
    }
    return render(request, 'FormPelanggan.html', context)

def KonfirmasiPembayaran(request):
    if request.method == "POST":
        form_utama = FormUtama(request.POST)
        if form_utama.is_valid():
            form_utama.save()
        return redirect(reverse("app2:kt"))
        
def KonfirmasiTerakhir(request):
    return render(request, 'KonfirmasiTerakhir.html')