from django.test import TestCase, Client, LiveServerTestCase
from .models import Isi
from .views import FormPelanggan, KonfirmasiPembayaran, KonfirmasiTerakhir
from django.shortcuts import render, redirect, reverse

# Create your tests here.
class Test(TestCase):
    def setUp(self):
        self.Url_kp = reverse("app2:kp")

    def template_home_ada(self):
        response = Client().get('app2/')
        self.assertEquals(response.status_code, 200)
    
    def template_home_bener(self):
        response = Client.get('/app2')
        self.assertTemplateUsed(response, "FormPelanggan.html")

    def test_buat_form(self):
        isi = Isi(
            nama_orang = "Bambang",
            nomor_hp = "08090348",
            alamat_email = "Bambang@gmail.com",
            jumlah_donasi = "10000000",
            nama_bank = "Bank BCA"
        )
        isi.save()
        self.assertEquals(Isi.objects.all().count(), 1)
    
    def test_posting(self):
        response = self.client.post(self.Url_kp, {
            "nama_orang" : "Bambang",
            "jumlah_donasi" : "1000000"
        })
        self.assertEquals(response.status_code, 302)